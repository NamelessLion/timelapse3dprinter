from io import open

def convert_time_lapse(root):
    try:
        flag_layers_count = False
        layers = 0
        layers_tmp = 0
        print(str(root))
        fichero = open(str(root), "r")
        gcode_converted = open('connect/convert/salida.gcode','w')  
        with fichero:
            for linea in fichero:
                gcode_converted.write(linea)
                if not flag_layers_count:
                    layers = get_layers(linea)
                    if not layers == None:
                        layers = int(layers)
                        flag_layers_count = True
                if get_layer(linea):
                    gcode_converted.write("M117 capa "+str((layers - (layers - layers_tmp)))+ " de "+str(layers)+"\n")
                    layers_tmp+=1
                if linea.__eq__("M400 ;Wait for moves to finish\n"):
                    gcode_converted.write("M117 Tomando foto\n")
                if linea.__eq__(";End of Gcode\n"):
                    gcode_converted.write("M118 E1 Fin\n")
        gcode_converted.close()
        return True
    except Exception as e:
        return str(type(e).__name__)

def get_layers(linea):
    tmp = ""
    for index, caracter in enumerate(linea):
        tmp += caracter
        if tmp.__eq__(";LAYER_COUNT:"):
            return linea[index+1:]
    return None
def get_layer(linea):
    tmp = ""
    for index, caracter in enumerate(linea):
        tmp += caracter
        if tmp.__eq__(";LAYER:"):
            return True
    return False