import serial
import time

def connect_printer(baudios):
    try:
        printer = serial.Serial('/dev/ttyUSB0', baudios)

        return printer
    except Exception as e:
        return None

def disconnect_printer(printer):
    try:
        printer.close()
        return True
    except Exception as e:
        return None

def read_printer(printer):
    answer = None
    while True:
        answer = printer.readline()
        answer = str(answer).strip('\n').strip('b')
        answer = answer[:len(answer)-3]
        answer += "'"
        if answer:
            break
    return answer
