import os
from threading import Timer

#si = os.system("adb shell dumpsys power | grep 'mHolding' > si.txt")

def get_devices():
    os.system("platform-tools/adb devices > connect/adb_shell_commands_photo/devices.txt")
    fichero = open("connect/adb_shell_commands_photo/devices.txt", "r")
    devices = []
    with fichero:
        for linea in fichero:
            if not linea.__eq__("List of devices attached\n") and not linea.__eq__('\n'):
                tmp = ''
                for c in linea:
                    if not c.__eq__('\t'):
                        tmp += c
                    else: break
                devices.append(tmp)
    return devices
def check_device(key):
    os.system("adb -s "+str(key)+" shell getprop > connect/adb_shell_commands_photo/check.txt || echo 'fail' > connect/adb_shell_commands_photo/check.txt ")
    fichero = open("connect/adb_shell_commands_photo/check.txt", "r")
    with fichero:
        for linea in fichero:
            if linea.__eq__("fail\n"):
                return False
    return True
def check_screen(key):
    flag_one = False
    flag_two = False
    with os.popen("adb -s "+key+" shell dumpsys power | grep 'mHolding'") as gg:
        for si in gg:
            if si.__eq__("  mHoldingWakeLockSuspendBlocker=true\n") or si.__eq__("  mHoldingWakeLockSuspendBlocker=false\n"):
                flag_one = True
            elif si.__eq__("  mHoldingDisplaySuspendBlocker=true\n"):
                flag_two = True
    return flag_one and flag_two

def send_command(cmd):
    os.system(str(cmd))