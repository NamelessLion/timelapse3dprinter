import threading
import tkinter
from connect.printer.printer_connect import connect_printer, disconnect_printer, read_printer
from connect.adb_shell_commands_photo.adb_printer import get_devices, check_device, send_command, check_screen
from tkinter import *
from tkinter import filedialog as FileDialog
from connect.convert import gcode_convert as gcode
import json
from threading import Timer
import time

def __main__():
    root = Tk()
    root.title("TimeLapse")
    root.resizable(0,0)
    root.protocol("WM_DELETE_WINDOW", lambda: close_window(root))
    data = json_read()
    #! background image in frame
    bg = PhotoImage(file="icon/chemstrans.png")
    #! Frame basic setup
    frame = Frame(root)
    frame.grid()
    frame.config(width=500, height=600)
    #! put backgroung image
    label1 = Label( root, image = bg) 
    label1.place(x = 50,y = 50)

    #! text show info config
    texto = Text(root)
    texto.grid()
    texto.config(width=30, height=5, state=DISABLED)
    texto.config(font=("Consolas",12), selectbackground="red", padx=5, pady=5)
    texto.place(x=100, y=300)
    
    set_text_text("GG", texto)

    #! input for baudios
    label = Label(root, text="Baud Rate")
    label.place(x=0, y=0)

    entry = Entry(root)
    entry.config(width=10)
    entry.place(x=80, y=0)

    button = Button(root, text="Connect", command=lambda: printer_connect(str(entry.get()), entry, texto))
    button2 = Button(root, text="Disconnect", command=lambda: printer_disconnect(entry, texto))
    button.grid()
    button.place(x=180, y=0)
    button2.grid()
    button2.place(x=280, y=0)


    #! menu for open gcode
    menubar = Menu(root)
    root.config(menu=menubar)
    filemenu = Menu(menubar, tearoff=0)
    filemenu.add_command(label="Abrir y convertir", command=lambda: open_gcode(data))
    filemenu.add_command(label="Clear console", command=lambda: clear_console(texto))
    menubar.add_cascade(label="Archivo", menu=filemenu)

    #! select device or cel
    button_scan_devices = Button(root, text="Scan devices", command=lambda: scan_devices(variable, w))
    button_scan_devices.grid()
    button_scan_devices.place(x=250, y=50)
    label_devices = Label(root, text="Devices")
    label_devices.place(x=0, y=50)

    variable = StringVar(root)
    variable.set(devices[0]) # default value
    w = OptionMenu(root, variable, *devices)
    w.grid()
    w.place(x=60, y=50)
    
    button_test_devices = Button(root, text="test device", command=lambda: callback(variable, texto))
    button_test_devices.grid()
    button_test_devices.place(x=380, y=50)
    #! put baudrates
    entry.insert(INSERT, str(data['baudios']))

    root.mainloop()
#! open json config
def json_read():
    with open('config/config.json') as json_file:
        data = json.load(json_file)
    return data
#! json write
def writte_json(data):
    with open('config/config.json', 'w') as outfile:
        json.dump(data, outfile)

#! open file and convert
def open_gcode(data):
    file = FileDialog.askopenfilename(
        initialdir=data['ruta'], 
        filetypes=(
            ("Ficheros gcode", "*.gcode"),
        ),
        title = "Abrir un fichero."
    )
    if file:
        path = get_path(str(file))
        data["ruta"] = path
        writte_json(data)
        gcode.convert_time_lapse(str(file))
        fichero = FileDialog.asksaveasfile(
            title="Guardar un fichero", mode='w', defaultextension=".gcode", initialfile="converted")
        if fichero is not None:
            gcode_converted = open('connect/convert/salida.gcode','r')
            fichero.write(gcode_converted.read())
            gcode_converted.close()
            fichero.close() 

#! get path form string
def get_path(path):
    tmp = ""
    path_absolute = ""
    for index, caracter in enumerate(path):
        tmp += caracter
        if caracter == '/' and index < len(path):
            path_absolute+=tmp
            tmp = ""
    return path_absolute

#! connect printer
def printer_connect(baudios, entry, texto):
    entry.config(state=DISABLED)
    set_text_text("Conectando...", texto)

    global conn
    conn = connect_printer(baudios)
    if conn.isOpen() and flag_device_connect:
        set_text_text("conectado :D", texto)
        t = Timer(2.0,lambda: read_serial(entry, texto))
        t.start()
    
#! printer disconect
def printer_disconnect(entry, texto):
    set_text_text("Desconectando...", texto)
    if disconnect_printer(conn): 
        set_text_text("Desconectado", texto)
        entry.config(state=NORMAL)
        global flag_connect_camera
        flag_connect_camera = False

#! method to set text in widget text    
def set_text_text(msg, texto):
    texto.configure(state=NORMAL)
    texto.insert("end", str(msg) + "\n")
    texto.see("end")
    texto.configure(state=DISABLED)
#! Clear console text
def clear_console(texto):
    texto.configure(state=NORMAL)
    texto.delete("1.0", "end")
    texto.configure(state=DISABLED)
#! read serial port
def read_serial(entry, texto):
    while conn.isOpen() is not False:
        si = read_printer(conn)
        set_text_text(str(si), texto)
        if si.__eq__("'echo:Potho'") or si.__eq__("'echo:Potho '"):
            global flag_shell_send
            flag_shell_send = True
            if flag_connect_camera:
                print("enter uno")
                send_command("platform-tools/adb -s "+str(actual_device)+" shell input tap 600 1000")
                send_command("platform-tools/adb -s "+str(actual_device)+" shell input keyevent 27")
                flag_shell_send = False
        if si.__eq__("'echo:Fin'"):
            printer_disconnect(entry, texto)
            break
    else:
        set_text_text("Madres, trata de nuevo...", texto)
        entry.config(state=NORMAL)
#! scan cell phone connect
def scan_devices(variable, w):
    global devices
    devices = get_devices()
    refresh(variable,w)
#! refersh devices
def refresh(var, network_select):
    # Reset var and delete all old options
    var.set(devices[0])
    network_select['menu'].delete(0, 'end')
    
    # Insert list of new options (tk._setit hooks them up to var)
    new_choices = devices
    for choice in new_choices:
        network_select['menu'].add_command(label=choice, command=tkinter._setit(var, choice))    
#! select event
def callback(selection, texto):
    global actual_device
    actual_device = selection.get()
    if check_device(selection.get()):
        set_text_text("Dispositivo encontrado", texto)
        set_text_text("iniciando hilo\nmantener pantalla encendida", texto)
        send_command("platform-tools/adb -s "+str(actual_device)+" shell am start -a android.media.action.STILL_IMAGE_CAMERA")
        global flag_connect_camera
        flag_connect_camera = True
        global flag_screen
        flag_screen = False
        global flag_device_connect
        flag_device_connect = True
        global t
        t = threading.Thread(target=si_hilo,name="unblock", args=(selection,))
        t.start()
    else:
        flag_device_connect = False
        set_text_text("error 404", texto)
#! init thread
def si_hilo(selection):
    global flag_screen
    flag_screen = True
    te = Timer(5.0, lambda: unblock_display(selection, pwd="pin of your phone"))
    te.start()

def unblock_display(selection, pwd):
    while flag_screen:
        if not check_screen(selection.get()):
            if not flag_shell_send:
                send_command("platform-tools/adb"+ 
                    " -s "+str(selection.get())+
                    " shell input keyevent 82 &&"+
                    " platform-tools/adb -s "+str(selection.get())+
                    " shell input keyevent 82 &&"+
                    " platform-tools/adb -s "+str(selection.get())+
                    " shell input text "+str(pwd)
                    )

def close_window(root):
    global flag_screen
    flag_screen = False
    root.destroy()

conn = None
devices = ['si']
flag_screen = False
flag_connect_camera = False
actual_device = ""
flag_device_connect = False
flag_shell_send = False
t = None
__main__()
